package com.navigation.drawer.activity;

import java.io.IOException;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class ProfilePage extends BaseActivity implements OnItemSelectedListener {
	
	String[] docs = { "Aadhar Card", "Liscence", "Passport", "PAN Card"};
	ImageView iv;
	private static int GALLERY_REQUEST = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getLayoutInflater().inflate(R.layout.activity_profile_page, frameLayout);
		
		/**
		 * Setting title and itemChecked  
		 */
		//mDrawerList.setItemChecked(position, true);
		//setTitle(listArray[position]);
		Spinner spin = (Spinner) findViewById(R.id.spinner1);  
		iv =(ImageView)findViewById(R.id.imageView1);
		
        spin.setOnItemSelectedListener(this); 
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,docs);  
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);  
        //Setting the ArrayAdapter data on the Spinner  
        spin.setAdapter(aa);
        
        iv.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
				photoPickerIntent.setType("image/*");
				startActivityForResult(photoPickerIntent, GALLERY_REQUEST);

		    }
			
		});
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK)
        switch (requestCode){
            case 1:
                Uri selectedImage = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    iv.setImageBitmap(bitmap);
                } catch (IOException e) {
                    //Log.i("TAG", "Some exception " + e);
                }
                break;
        }
    }

	

}
