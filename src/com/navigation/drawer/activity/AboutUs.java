package com.navigation.drawer.activity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.webkit.WebView;

public class AboutUs extends BaseActivity {
	WebView wv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/**
		 * Adding our layout to parent class frame layout.
		 */
		getLayoutInflater().inflate(R.layout.activity_about_us, frameLayout);
		
		/**
		 * Setting title and itemChecked  
		 */
		mDrawerList.setItemChecked(position, true);
		//setTitle(listArray[position]);
		
		wv = (WebView)findViewById(R.id.webView1);
		wv.loadUrl("file:///android_asset/aboutus.html");
	}

	

}
