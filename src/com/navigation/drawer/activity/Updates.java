package com.navigation.drawer.activity;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

public class Updates extends BaseActivity {
	
	ImageView iv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getLayoutInflater().inflate(R.layout.activity_updates, frameLayout);
		
		/**
		 * Setting title and itemChecked  
		 */
		mDrawerList.setItemChecked(position, true);
		setTitle(listArray[position]);
		
		iv = (ImageView)findViewById(R.id.imageView2);
		
		iv.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent open = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/oohlala205/"));
		        startActivity(open);
			}
		});
	}

	

}
