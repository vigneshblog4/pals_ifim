package com.navigation.drawer.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author dipenp
 *
 */
public class Subscription extends BaseActivity {
	
	Button b1,b2;
	TextView t1,t2;
	WebView wv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/**
		 * Adding our layout to parent class frame layout.
		 */
		getLayoutInflater().inflate(R.layout.subscription_layout, frameLayout);
		
		/**
		 * Setting title and itemChecked  
		 */
		mDrawerList.setItemChecked(position, true);
		setTitle(listArray[position]);
		
		b1=(Button)findViewById(R.id.com);
		b2=(Button)findViewById(R.id.noncom);
		t1=(TextView)findViewById(R.id.t1);
		t2=(TextView)findViewById(R.id.t2);
		wv=(WebView)findViewById(R.id.webView1);
		
		wv = (WebView)findViewById(R.id.webView1);
		wv.loadUrl("file:///android_asset/community.html");
		
		b1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				wv.loadUrl("file:///android_asset/community.html");
			}
		});
		b2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				wv.loadUrl("file:///android_asset/noncommunity.html");
			}
		});
	}
}
