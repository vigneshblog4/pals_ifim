package com.navigation.drawer.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author dipenp
 *
 */
public class TriviaPage extends BaseActivity {

	RadioGroup rg;
	RadioButton r1,r2,r3,r4;
	TextView quesNo,ques, timer;
	Button prev,next, submit;
	String[] q = {"Who invented Bicycle?","Who is called as the father of bicycle industry?",
			"Walking or riding a bicycle protects the environment because",
			"In which city india�s first smartcard-based Public Bicycle Sharing (PBS) initiative �Trin Trin� has launched?",
			"Zero was invented by?"};
	
	
	
	 
	
	String[] o1 = {"Leo H Baekeland","Gottlieb Daimler","Harmful pollutants are not produced",
			"Mysore","An Unknown Indian"};
	String[] o2 = {"Karl Benz","Dennis Johnson","It is good exercise",
			"Jabalpur","Bhaskar"};
	String[] o3 = {"Evangelista Torricelli","Alfred Nobel","It saves money",
			"Aurangabad","Varahmihir"};
	String[] o4 = {"Kirkpatrick Macmillan","James Starley","None of the above",
			"Rupnagar","Aryabhatta"};
	
	private CountDownTimer countDownTimer;
	private boolean timerHasStarted = false;
	private final long startTime = 30 * 1000;
	private final long interval = 1 * 1000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/**
		 * Adding our layout to parent class frame layout.
		 */
		getLayoutInflater().inflate(R.layout.trivia_layout, frameLayout);
		
		/**
		 * Setting title and itemChecked  
		 */
		mDrawerList.setItemChecked(position, true);
		setTitle(listArray[position]);
		
		
		quesNo = (TextView)findViewById(R.id.queNo);
		ques = (TextView)findViewById(R.id.question);
		timer = (TextView)findViewById(R.id.timer);
		prev = (Button)findViewById(R.id.prev);
		next = (Button)findViewById(R.id.next);
		submit = (Button)findViewById(R.id.submit);
		rg = (RadioGroup)findViewById(R.id.radioGroup);
		r1 =(RadioButton)findViewById(R.id.radioButton1);
		r2 =(RadioButton)findViewById(R.id.radioButton2);
		r3 =(RadioButton)findViewById(R.id.radioButton3);
		r4 =(RadioButton)findViewById(R.id.radioButton4);
		
		rg.clearCheck();
		
		
		
		ques.setText(q[0]);
		r1.setText(o1[0]);
		r2.setText(o2[0]);
		r3.setText(o3[0]);
		r4.setText(o4[0]);
		
		
		
		countDownTimer = new MyCountDownTimer(startTime, interval);
		timer.setText(String.valueOf(startTime / 1000));
		countDownTimer.start();
		timerHasStarted = true;
		
		rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (null != rb && checkedId > -1) {
                	
                	//2131230739
                	
                }

            }
        });
		
		prev.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(quesNo.getText().toString().equals("5")){
					quesNo.setText("4");
					ques.setText(q[3]);
					r1.setText(o1[3]);
					r2.setText(o2[3]);
					r3.setText(o3[3]);
					r4.setText(o4[3]);
				}
				
				else if(quesNo.getText().toString().equals("4")){
					quesNo.setText("3");
					ques.setText(q[2]);
					r1.setText(o1[2]);
					r2.setText(o2[2]);
					r3.setText(o3[2]);
					r4.setText(o4[2]);
				}
				
				else if(quesNo.getText().toString().equals("3")){
					quesNo.setText("2");
					ques.setText(q[1]);
					r1.setText(o1[1]);
					r2.setText(o2[1]);
					r3.setText(o3[1]);
					r4.setText(o4[1]);
				}
				
				else if(quesNo.getText().toString().equals("2")){
					quesNo.setText("1");
					ques.setText(q[0]);
					r1.setText(o1[0]);
					r2.setText(o2[0]);
					r3.setText(o3[0]);
					r4.setText(o4[0]);
				}
				
			}
		});
		
		next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(quesNo.getText().toString().equals("1")){
					quesNo.setText("2");
					ques.setText(q[1]);
					r1.setText(o1[1]);
					r2.setText(o2[1]);
					r3.setText(o3[1]);
					r4.setText(o4[1]);
				}
				
				else if(quesNo.getText().toString().equals("2")){
					quesNo.setText("3");
					ques.setText(q[2]);
					r1.setText(o1[2]);
					r2.setText(o2[2]);
					r3.setText(o3[2]);
					r4.setText(o4[2]);
				}
				
				else if(quesNo.getText().toString().equals("3")){
					quesNo.setText("4");
					ques.setText(q[3]);
					r1.setText(o1[3]);
					r2.setText(o2[3]);
					r3.setText(o3[3]);
					r4.setText(o4[3]);
				}
				
				else if(quesNo.getText().toString().equals("4")){
					quesNo.setText("5");
					ques.setText(q[4]);
					r1.setText(o1[4]);
					r2.setText(o2[4]);
					r3.setText(o3[4]);
					r4.setText(o4[4]);
				}
			}
		});
		 submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	public class MyCountDownTimer extends CountDownTimer {
		  public MyCountDownTimer(long startTime, long interval) {
		   super(startTime, interval);
		  }

		  @Override
		  public void onFinish() {
		   timer.setText("Your Time's up!");
		  }

		  @Override
		  public void onTick(long millisUntilFinished) {
		   timer.setText("" + millisUntilFinished / 1000);
		  }
		 }
}
